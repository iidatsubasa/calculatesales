package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_ASCENDING_ORDER = "売上ファイルが連番になっていません";
	private static final String FILE_NOT_FORMAT = "のフォーマットが不正です";
	private static final String FILE_NOT_BRANCH_CODE = "の支店コードが不正です";
	private static final String FILE_NOT_COMMODITY_CODE = "の商品コードが不正です";
	private static final String OVER_TOTAL_FEE = "合計金額が10桁を超えました";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		//商品コードと商品名を保持するmap
		Map<String, String> commodityNames = new HashMap<>();
		//商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "^[0-9]{3}", "支店")) {
			return;
		}
		//商品定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "^[a-zA-Z0-9]{8}", "商品")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		String totalLine;
		//売上ファイル名比較用一時保存先
		BufferedReader br = null;

		for (int i = 0; i < files.length; i++) {
			//売上ファイル名取得
			String totalName = files[i].getName();

			//売上ファイルがファイルかつ数字8桁かつ拡張子.rcdであるか判定
			if (files[i].isFile() && totalName.matches("[0-9]{8}\\.rcd")) {
				rcdFiles.add(files[i]);
			}
		}
		//売上ファイルが連番かどうかの判定

		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));
			if ((latter - former) != 1) {
				System.out.println(FILE_NOT_ASCENDING_ORDER);
				return;
			}
		}

		//集計処理
		for (int i = 0; i < rcdFiles.size(); i++) {
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				//売上ファイルの保存先
				ArrayList<String> totalList = new ArrayList<>();

				//1行ずつ読み込み、totalListへ追加
				while ((totalLine = br.readLine()) != null) {
					totalList.add(totalLine);
				}

				//売上ファイルが3行か判定
				if (totalList.size() != 3) {
					System.out.println(rcdFiles.get(i) + FILE_NOT_FORMAT);
					return;
				}

				//支店コードが存在するか判定
				if (!branchSales.containsKey(totalList.get(0))) {
					System.out.println(rcdFiles.get(i) +  FILE_NOT_BRANCH_CODE);
 					return;
 				}

				//商品コードが存在するか判定
				if (!commoditySales.containsKey(totalList.get(1))) {
					System.out.println(rcdFiles.get(i) + FILE_NOT_COMMODITY_CODE);
					return;
				}

				//売上金額が数字であるか判定
				if (!totalList.get(2).matches("^[0-9]*$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//集計
				long fileSale = Long.parseLong(totalList.get(2));
				Long saleBranchAmmount = branchSales.get(totalList.get(0)) + fileSale;
				Long saleCommodityAmmount = commoditySales.get(totalList.get(1)) + fileSale;

				//合計金額が10桁を超えないか判定
				if (saleBranchAmmount >= 1000000000L || saleCommodityAmmount >= 1000000000L) {
					System.out.println(OVER_TOTAL_FEE);
					return;
				}

				//集計後、各Mapへ保存
				branchSales.put(totalList.get(0), saleBranchAmmount);
				commoditySales.put(totalList.get(1), saleCommodityAmmount);

			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if (br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		// 商品別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名または商品コードと商品名を保持するMap
	 * @param 支店または商品コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> nameMap,
			Map<String, Long> saleMap, String regex, String defin) {
		BufferedReader br = null;

		//読み込み処理
		try {
			File file = new File(path, fileName);

			//ファイルの存在確認
			if (!file.exists()) {
				System.out.println(defin + FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");

				//ファイルフォーマットの判定
				if (items.length != 2 || !items[0].matches(regex)) {
					System.out.println(defin + FILE_INVALID_FORMAT);
					return false;
				}

				//各Mapへ保存
				nameMap.put(items[0], items[1]);
				saleMap.put(items[0], 0L);
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名または商品コードと商品名を保持するMap
	 * @param 支店または商品コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> nameMap,
			Map<String, Long> saleMap) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;
		try {
			//branch_outへ書き込み
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//branchNamesの全てのkeyを取得
			for (String key : nameMap.keySet()) {
				//書き込み
				bw.write(key + "," + nameMap.get(key) + "," + saleMap.get(key));
				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (bw != null) {
				try {
					// ファイルを閉じる
					bw.close();

				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}
